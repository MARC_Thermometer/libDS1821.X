/*! \file  DS1821_PollStatus.c
 *
 *  \brief Wait for the DS1821 to become ready
 *
 *
 *  \author jjmcd
 *  \date January 17, 2015, 1:25 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "DS1821.h"

/*! DS1821_PollStatus - Wait for the DS1821 to become ready */

/*! DS1821_PollStatus() - Wait for the DS1821 to become ready
 *  DS1821_PollStatus() continually sends DS_READSTATUS commands
 *  to the DS1821, and then reads the response from the DS1821 until
 *  the sensor responds with DS_DONE.
 *
 * Pseudocode:
 * \code
 * done = 0
 * while ( !done )
 *     Send a master reset
 *     Send a DS_READSTATUS command
 *     Read the response from the DS1821
 *     if the response is DS_DONE
 *         done = 1
 * \endcode
 * \callgraph
 * \callergraph
 * \param nDSnum int - Number of the DS1821 to poll
 * \return none
 */
void DS1821_PollStatus(int nDSnum)
{
    int done;

    done = 0;
    while ( !done )
    {
        DS1821_MasterReset(nDSnum);
        DS1821_Command( nDSnum, DS_READSTATUS );
        if ( DS1821_Read( nDSnum ) & DS_DONE )
            done = 1;
    }
}
