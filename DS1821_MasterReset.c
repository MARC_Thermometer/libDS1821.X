/*! \file  DS1821_MasterReset.c
 *
 *  \brief Send a master reset pulse to the DS1821 digital thermostat
 *
 *
 *  \author jjmcd
 *  \date January 17, 2015, 1:20 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include "DS1821.h"

/*! DS1821_MasterReset - Send a master reset */

/*! DS1821_MasterReset() sends a master reset pulse to the DS1821.
 *  The master reset is initiated by lowering the line for 600
 *  microseconds.  About 30 microseconds after the line is released,
 *  the sensor will respond by lowering the line for 120us.
 *
 * Pseudocode:
 * \code
 * Calculate the inverted mask
 * Lower the line
 * Initialize the timer to 60us
 * Wait for the interrupt flag
 * Raise the line
 * Wait until the line goes low
 * Wait while the line is low
 * \endcode
 * \callergraph
 * \param nDSnum int - Number of the DS1821 to reset
 * \return none
 */
void DS1821_MasterReset(int nDSnum)
{
    unsigned int uImask;

    // Calculate the inverted mask
    uImask = uDSmask[nDSnum] ^ 0xffff;

    // Clear the latch bit
    *(uDSport[nDSnum]+1) &= uImask;
    // Wait 600us
    PR1821 = T600US;
    /* Reset the timer */
    TMR1821 = 0;
    IF1821 = 0;
    // Wait for timer to expire
    while ( !IF1821 )
	;
    // Set the latch bit
    *(uDSport[nDSnum]+1) |= uDSmask[nDSnum];

    // Since the port is open collector, we can see the state even if not an input
    while ( (*(uDSport[nDSnum])&uDSmask[nDSnum]) )  // wait while high
	;                    // Should be 15-60us (Measured 30)
    while ( !(*(uDSport[nDSnum])&uDSmask[nDSnum]) ) // wait while low
	;                    // should be 60-240 us (measured 117)
    return;

}
