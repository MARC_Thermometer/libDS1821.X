#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=a
DEBUGGABLE_SUFFIX=a
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/libDS1821.X.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=a
DEBUGGABLE_SUFFIX=a
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/libDS1821.X.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=DS1821_MasterReset.c DS1821_Read.c DS1821_PollStatus.c DS1821_Command.c DS1821_ReadTemp.c DS1821_Initialize.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/DS1821_MasterReset.o ${OBJECTDIR}/DS1821_Read.o ${OBJECTDIR}/DS1821_PollStatus.o ${OBJECTDIR}/DS1821_Command.o ${OBJECTDIR}/DS1821_ReadTemp.o ${OBJECTDIR}/DS1821_Initialize.o
POSSIBLE_DEPFILES=${OBJECTDIR}/DS1821_MasterReset.o.d ${OBJECTDIR}/DS1821_Read.o.d ${OBJECTDIR}/DS1821_PollStatus.o.d ${OBJECTDIR}/DS1821_Command.o.d ${OBJECTDIR}/DS1821_ReadTemp.o.d ${OBJECTDIR}/DS1821_Initialize.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/DS1821_MasterReset.o ${OBJECTDIR}/DS1821_Read.o ${OBJECTDIR}/DS1821_PollStatus.o ${OBJECTDIR}/DS1821_Command.o ${OBJECTDIR}/DS1821_ReadTemp.o ${OBJECTDIR}/DS1821_Initialize.o

# Source Files
SOURCEFILES=DS1821_MasterReset.c DS1821_Read.c DS1821_PollStatus.c DS1821_Command.c DS1821_ReadTemp.c DS1821_Initialize.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/libDS1821.X.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=24FV16KM202
MP_LINKER_FILE_OPTION=,--script=p24FV16KM202.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/DS1821_MasterReset.o: DS1821_MasterReset.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/DS1821_MasterReset.o.d 
	@${RM} ${OBJECTDIR}/DS1821_MasterReset.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  DS1821_MasterReset.c  -o ${OBJECTDIR}/DS1821_MasterReset.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/DS1821_MasterReset.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/DS1821_MasterReset.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/DS1821_Read.o: DS1821_Read.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/DS1821_Read.o.d 
	@${RM} ${OBJECTDIR}/DS1821_Read.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  DS1821_Read.c  -o ${OBJECTDIR}/DS1821_Read.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/DS1821_Read.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/DS1821_Read.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/DS1821_PollStatus.o: DS1821_PollStatus.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/DS1821_PollStatus.o.d 
	@${RM} ${OBJECTDIR}/DS1821_PollStatus.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  DS1821_PollStatus.c  -o ${OBJECTDIR}/DS1821_PollStatus.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/DS1821_PollStatus.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/DS1821_PollStatus.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/DS1821_Command.o: DS1821_Command.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/DS1821_Command.o.d 
	@${RM} ${OBJECTDIR}/DS1821_Command.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  DS1821_Command.c  -o ${OBJECTDIR}/DS1821_Command.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/DS1821_Command.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/DS1821_Command.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/DS1821_ReadTemp.o: DS1821_ReadTemp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/DS1821_ReadTemp.o.d 
	@${RM} ${OBJECTDIR}/DS1821_ReadTemp.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  DS1821_ReadTemp.c  -o ${OBJECTDIR}/DS1821_ReadTemp.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/DS1821_ReadTemp.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/DS1821_ReadTemp.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/DS1821_Initialize.o: DS1821_Initialize.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/DS1821_Initialize.o.d 
	@${RM} ${OBJECTDIR}/DS1821_Initialize.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  DS1821_Initialize.c  -o ${OBJECTDIR}/DS1821_Initialize.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/DS1821_Initialize.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/DS1821_Initialize.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/DS1821_MasterReset.o: DS1821_MasterReset.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/DS1821_MasterReset.o.d 
	@${RM} ${OBJECTDIR}/DS1821_MasterReset.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  DS1821_MasterReset.c  -o ${OBJECTDIR}/DS1821_MasterReset.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/DS1821_MasterReset.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/DS1821_MasterReset.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/DS1821_Read.o: DS1821_Read.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/DS1821_Read.o.d 
	@${RM} ${OBJECTDIR}/DS1821_Read.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  DS1821_Read.c  -o ${OBJECTDIR}/DS1821_Read.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/DS1821_Read.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/DS1821_Read.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/DS1821_PollStatus.o: DS1821_PollStatus.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/DS1821_PollStatus.o.d 
	@${RM} ${OBJECTDIR}/DS1821_PollStatus.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  DS1821_PollStatus.c  -o ${OBJECTDIR}/DS1821_PollStatus.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/DS1821_PollStatus.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/DS1821_PollStatus.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/DS1821_Command.o: DS1821_Command.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/DS1821_Command.o.d 
	@${RM} ${OBJECTDIR}/DS1821_Command.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  DS1821_Command.c  -o ${OBJECTDIR}/DS1821_Command.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/DS1821_Command.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/DS1821_Command.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/DS1821_ReadTemp.o: DS1821_ReadTemp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/DS1821_ReadTemp.o.d 
	@${RM} ${OBJECTDIR}/DS1821_ReadTemp.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  DS1821_ReadTemp.c  -o ${OBJECTDIR}/DS1821_ReadTemp.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/DS1821_ReadTemp.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/DS1821_ReadTemp.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/DS1821_Initialize.o: DS1821_Initialize.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/DS1821_Initialize.o.d 
	@${RM} ${OBJECTDIR}/DS1821_Initialize.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  DS1821_Initialize.c  -o ${OBJECTDIR}/DS1821_Initialize.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/DS1821_Initialize.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/DS1821_Initialize.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: archive
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/libDS1821.X.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/libDS1821.X.${OUTPUT_SUFFIX} 
	${MP_AR} $(MP_EXTRA_AR_PRE)  -omf=elf -r dist/${CND_CONF}/${IMAGE_TYPE}/libDS1821.X.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/libDS1821.X.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/libDS1821.X.${OUTPUT_SUFFIX} 
	${MP_AR} $(MP_EXTRA_AR_PRE)  -omf=elf -r dist/${CND_CONF}/${IMAGE_TYPE}/libDS1821.X.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
